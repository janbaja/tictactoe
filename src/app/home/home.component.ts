import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

	winner: string;

	constructor(public dialog: MatDialog) {}

	openDialog() {
	  this.dialog.open(DialogComponent, {
		data: { winner: this.winner }
	  });
	}

	angForm = new FormGroup({
		names: new FormArray([
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
			new FormControl(''),
		])
	});

	addNumberField() {
		this.onFormSubmit();
	}
	get names(): FormArray {
		return this.angForm.get('names') as FormArray;
	}
	onFormSubmit(): void {

		let probability = [
			[0, 1, 2],
			[3, 4, 5],
			[6, 7, 8],
			[0, 3, 6],
			[1, 4, 7],
			[2, 5, 8],
			[0, 4, 8],
			[2, 4, 6]
		];

		for (const pro of probability) {
			let a = this.names.at(pro[0]).value;
			let b = this.names.at(pro[1]).value;
			let c = this.names.at(pro[2]).value;
			
			if (this.names.at(pro[0]).value !== ''){
				if (this.names.at(pro[0]).value % 2 === 0) {
					a = 'o';
				} else {
					a = 'x';
				}
			}
			if (this.names.at(pro[1]).value !== '') {
				if (this.names.at(pro[1]).value % 2 === 0) {
					b = 'o';
				} else {
					b = 'x';
				}
			}
			if (this.names.at(pro[2]).value !== '') {
				if (this.names.at(pro[2]).value % 2 === 0) {
					c = 'o';
				} else {
					c = 'x';
				}
			}

			if (a === b &&
				b === c &&
				a !== null) {
				
				if (b) {
					this.winner = b;
					this.openDialog();
				}

				return;
			}
		}

	}

	ngOnInit(): void {
	}

}

@Component({
	selector: 'app-dialog-design',
	templateUrl: '../dialog/dialog.component.html'
})
export class DialogComponent {}